# House things

Furnace filter size: 16x25x1


# 2016 New Holland Workmaster 37

### Notes
Bought in Fall of 2019 for $14,300. Manual bought from Cherokee Garage for $25. S/N: 2271001799

### Consumables
- Oil: 1 gal synthetic 10W-30. Change every 300 hours.
- Filter: MT40283380 (can purchase from Cherokee Garage in Colby). And possibly WIX 57000

### History
- 5-20
    - Epoxied left (from driver's perspective) headlight back on to hood, previous owner must've broken it
- 6-20
    - Oil changed with Shell T4 10W-30 synthentic and filter from new holland dealership
    - Greased bearings that I could see without manual to guide me.
    - Subsequently bought manual for $25 from cherokee garage and greased the rest.

----------------------

# Toro Zero Turn

### Consumables

- Oil: SAE 10W-30. Check @ 1.9qt/1.8L with oil filter replacement
- [Oil Filter Kawasaki 49065-0721](https://a.co/d/dQeuiTz)
- Spark plug: NGK BPR4ES
- Deck belt: 141.5" x 1/2"
    - I bought one at Menards for $34 in 2023 which was 141.7" x 1/2" and it seems to be working fine.

### Notes

- I just replaced a battery that had only been used for a few years (2018-2020). I never unhooked it and took it inside in the fall, but that would probably increase its life. I don't have that problem with the fourwheeler I think because I run it throughout the winter.

### History

- Bought brand new 09-2015
- Oil & filter changed: spring 2016
- Oil & filter changed: spring 2017
- Oil & filter changed. Blades sharpened: spring 2018
- Oil & filter changed. Blades sharpened. Mower belt replaced: spring 2019
- Oil & filter changed. Blades sharpened: spring 2020
- Oil & filter changed. Blades sharpened. New battery: spring 2021
- Oil changed. Blades sharpened. Ordered wrong filter: spring 2022
- Oil & filter changed. Blades sharpened, deck belt replaced (after breaking): spring 2023
- Oil changed. Blades sharpened. Spring 2024

----------------------

# 2008 Yamaha Wolverine

### Consumables

- Oil: Yamalube 4 (10W30) or SAE 10W30
    - 2.3L/2.43Qt without filter replacement
    - 2.4L/2.54Qt with filter replacement
- [Fram Extra Guard PH6017A](https://a.co/d/4zCghG4)
- Spark plug: NGK/DR8EA (18mm socket)

### Notes

- Be sure to apply a thin coat of oil to the oil filter seal.
- Reserve gas tank switch: since I could never tell, I looked at the manual. Down is "ON", left is "OFF", and up is "RES".

### History

- Oil & filter changed 12-2015
- Oil, filter & spark plug changed 06-2019
- Oil & filter changed 04-2021
- Oil & filter changed 05-2023. Spark plug cleaned and tightened beacause it wasn't starting. That fixed it.
- Spark plug changed 11-2023. $3.99 at O'Reilly
