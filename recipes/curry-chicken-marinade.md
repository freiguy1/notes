# Curried Chicken Marinade

Found on the package of a curry seasoning container.

### Ingredients

- 2 Tbs olive oil
- 2 tsp curry
- 1 tsp garlic salt
- 1 tsp onion powder
- 1 tsp paprika
- 3-4 chicken breasts (1.5 lbs)

### Instructions

1. Mix all, put in bag for at least half hour.
2. Grill chicken breasts for 5-6 min per side medium heat.
3. Goes well w/ grilled vegetables and rice
