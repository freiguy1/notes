<style>
    kf-notes {
        filter: brightness(65%);
    }
</style>

# Sourdough Sandwich Loaf

### Ingredients - see sources for substitution options

#### Pre-Ferment - Make 8-24 hours in advance
- 1/2 cup sourdough starter <kf-notes>it needs to have doubled, but can be /falling and needing to be fed, it does not need to be at optimal bread baking state</kf-notes>
- 1/2 cup warm water
- 2/3 cups all-purpose flour

#### Dough
- Pre-ferment from above
- 1 tablespoon butter
- 1 tablespoon maple syrup <kf-notes>or 1 tablespoon honey or 2 tablespoon sugar</kf-notes>
- 1 teaspoon salt
- 3/4 cup milk
- 2 1/4 cup all purpose flour <kf-notes>add more (up to 2 3/4 cup as needed)</kf-notes>
- Butter to brush crust after baking

### Instructions

#### PRE-FERMENT
1. Mix the pre-ferment of sourdough starter, flour, and water up 8-24 hours before you want to bake. The longer beforehand, the more sour your end product will be. If I plan to mix the dough in the morning, I make it before bed. Cover with plastic wrap or a plastic bag and let sit on the counter.

#### START THE DOUGH
1. Melt butter, honey, and salt on low in a saucepan. When it's melted and combined, turn off the heat, add your milk, and stir to combine. With a thermometer or your finger, test the temperature of the mixture. By thermometer, it should be no more than 105F. By your finger...you should be able to comfortably hold it in for 10 seconds. If it's not this warm, turn the heat back on to warm it. 
2. Add the warm liquid to your mixer (or bowl you plan to mix the dough in) and add your pre-ferment.
3. Stir to combine; it's not going to combine super well until you start adding flour.
4. Add your flour, starting on the low end, and mix the dough, adding more flour as needed just until it's combined, but not a cohesive, nice dough.
5. Let sit 15-30 minutes, then knead the dough.
6. This dough takes more kneading than most- my mixer kneads on low for 10 minutes, so if kneading by hand, you're going to knead about 10-15 minutes.
7. If the dough sticks to the bowl or your hands, add a little more flour, but try not to add too much. It's not a stiff dough, it's on the softer side. Try wetting your hands/counter for kneading if it's sticking and you've already added a lot more flour. 

#### RISING

##### Bake in One Day Method
1. Cover your dough with plastic wrap or a plastic bag and let rise 2-3 hours until it looks like it's kind of doubled (don't overthink it, as long as it's 1.5'd its original size).
2. Punch your dough down, give it a few kneads. Yes, you are letting it rise twice before shaping. For more on this, read the blog post above the recipe card.
3. Cover with plastic wrap or a plastic bag and let sit 2-3 hours until it's doubled. To test if your dough has doubled, dip your finger in flour and poke the dough. If the dough bounces back- it's got more to give and needs to keep rising. If the dough stays indented, it's given all it has to give and you can proceed. 

##### Overnight Method
1. Cover your dough with plastic wrap or a plastic bag and let sit overnight. It will be gigantic and beautiful in the morning. Skip the second rise with the overnight method. In my extensive testing of this recipe, I have found if you are doing an overnight rise, you can skip the autolyze rest before kneading AND skip the first punch down and rise. Just mix, knead, let it rise, punch down and shape in the morning.

#### SHAPING LOAF
1. Dump your dough onto a lightly floured counter, press out with your hands to make a rectangle roughly the size of your two hands flat, side by side (roughly 6"x10"). Fold the dough like you were folding a piece of paper to go in an envelope, flip seam side down and let rest 10 minutes.
2. Starting at the bottom, roll the dough into a log tucking the ends underneath. Transfer to the loaf pan. Watch the [Video](https://youtu.be/dN8hoIMEqRM)!
3. Transfer to buttered loaf pan. Cover with tea towel and let rise 2-3 hours. The dough is ready when the center rises to about 1-inch or more above the rim. 
4. Preheat oven to 500 degrees.
5. Slash the top of the dough with a sharp knife to allow for expansion.
6. Once oven is preheated, place loaf into oven and change oven temp to 375F
7. Bake for 20 minutes, flip around and bake another 20 minutes.
8. After the second round of 20 minutes (40 min total) check the internal temp of your loaves- this is the most reliable way to know your bread is ready. Your bread should be 190-200F, if not put it in for another 5 minutes.
9. When your bread is done, take it out of the oven, leave it in the loaf pan to cool, and brush butter on the top. This can be melting and using a brush or just rubbing a stick of butter over the top. This step is technically optional but it REALLY makes a nice soft crust that stays soft.
10. Now the easy part...enjoy your bread! I prefer to slice it all right away. Anything you wont eat within 2-3 days pop in freezer in a bag once its cooled. 

#### OTHER NOTES
EXCESS EGGS IN YOUR HOME?! You can swap ¼ cup of milk for an egg! Add the egg in step 3. It adds an awesome boost to your dough and when you've got extra eggs, why not?!


[Main Source](https://venisonfordinner.com/kates-soft-sourdough-master-recipe/)

[Secondary Source](https://www.theclevercarrot.com/2020/04/easy-sourdough-sandwich-bread/)
