This soup recipe comes from the Taste of Home slowcooker book. We've modified it slightly!

## Ingredients
- 1-1.5 lb beef/venison stew meat cut into 1/2" cubes
- 1 can (28 oz) diced tomatoes (we prefer petite diced)
- 1 med onion chopped
- 2 T minced dried parsley
- 2 1/2 t salt optional
- 1 1/2 t ground thyme
- 1 beef boullion cube
- 1/2 t pepper
- 6 c water
- 1 sm zucchini halved/quartered and thinly sliced
- 1 sm yellow squash halved/quartered and thinly sliced
- 2 c chopped cabbage
- 1 can (15 oz) garanzo beans or chickpeas, rinsed anIn last 20 min, cook noodles in a separate pot
7. Serve soup with noodles and grated parmesan cheese drained
- 1 box elbow macaroni
- grated Parmesan cheese

## Instructions

1. Hot cast iron pan with oil, brown meat. Salt and pepper a bit
2. Put in 5qt slow cooker
3. Deglaze with water or red wine, put in slow cooker
4. Put tomatoes, onion, parsley, salt (if desired), thyme, boullion, pepper and water in slow cooker. Cook on high for 4h or low for 7-9h
5. Add zucchini, squash, cabbage, garbanzo beans to the slow cooker or until veggies are tender
6. In last 20 min, cook noodles in a separate pot
7. Serve soup with noodles and grated parmesan cheese
