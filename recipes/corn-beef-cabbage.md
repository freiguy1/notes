# Corned Beef & Cabbage

On St Patty's day, I really enjoy a cooked corn beef with cabbage and potatoes.  However, since I don't make it that often, I thought I'd take some notes.

![](../assets/images/corned-beef-cabbage.jpg)

### Ingredients

1. One corn beef (which should come w/ a seasoning packet). This year was 1.7 lb.
2. One head of cabbage
3. 8 baby red potatoes (switched to yukon and loved the smoothness)
4. Like 8 Carrots
5. Water

### Instructions

1. Chop poatoes and carrots to desired size.
2. Put thawed corned beef in 5 qt crock pot.
3. Put chopped potatoes around beef so they're next to the sides of the crock pot if possible. And carrots can go on top.
4. Submerge at least the meat and potatoes in water.
5. Put on seasoning packet.
6. Low for 5 hours.
7. Chop cabbage and put on top
8. Low for another 3 hours.
9. Pull meat out and chop up perpindicular to grain in half inch pieces.
10. Put meat back in, mix and serve.

---
---

### Instant Pot Version

#### Ingredients

- 4 lb corned beef
- Medium bag of carrots
- 8 small/4 medium or large potatoes
- 1 cabbage

#### Instructions

- Put corned beef and juices in IP and almost cover it with water
- Sprinkle seasoning packet over
- Meat setting for 90 minutes & quick release steam
- 15 min before meat is done, peel carrots, wash and cut potatoes into 1 1/2" pieces, and cut cabbage into rubix cube size pieces (or smaller)
- When meat is done, quick release, brush seasonings off meat back into liquid - meat doesn't need them anymore
- Place meat on baking sheet and quickly cover w/ aluminum foil & place in oven or microwave to keep warm
- Place all veggies in liquid from cooking meat
- Manual setting for 4 minutes, quick release right away (getting closer!)
    - 4 min was still a little soft, for me, but I think 3 is hard. Maybe 3 minutes with a couple minutes of natural release
- Right before veggies done, take out meat, slice against the grain and serve with veggies!
