# Grandma's Cucumber Salad

Disclaimer: not from any of our grandmas. "Grandma's" was just in the name of the recipe. Found in a random magnet calendar from the mail. Kaytee and I both really liked it.

### Ingredients
- 2 t salt
- 1/2 t ground pepper
- 3 T sugar (consider cutting?)
- 3/4 c white vinegar
- 2/3 cup water
- 2 cucumbers thinly sliced
- 1 large clove garlic crushed
- 1/4 green bell pepper thinly sliced
- 1/4 red onion thinly sliced
- Optional: a few fresh dill sprigs

### Instructions

1. Mix together salt, pepper, sugar, vinegar and water until dry ingredients are dissolved.
2. Add the vegetables to the mixture and let sit overnight.
3. Serve as a side dish or pop it into a plastic storage bag and take it to the beach for a healthy snack!
