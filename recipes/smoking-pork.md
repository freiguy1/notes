# Smoking Pork Butt or Shoulder

Over the last couple years I've been toying with smoking meat on the weber kettle grill. It seems very delicious each time we do it, and there are steps in the whole process that I seem to forget each time so I had better write it down and tweak it as I learn more!

A ~5lb piece of meat lasts many meals for the family. If it seems helpful, I'll try to record how many meals/sandwiches per pound we get.

Remember smoking is a long process, so you can't like smoke a pork butt for dinner in a couple hours. It's a couple days of work.

Goes well with pickled red onions and/or coleslaw. Recipe down below for slaw.

### Ingredients

- 4-7 lb pork butt/shoulder. A 7-10 lb butt will take more like 12h of total cooking. A 12 lb might take 14 hours.
- Store-bought rub (carne asada or mccormick bbq rub)
- buns
- Fuel:
  - Charcoal and blocks of hardwood (oak, apple, hickory)

# Directions for smoking with [Slow 'N Sear](https://snsgrills.com/collections/slow-n-sear/products/slow-n-sear-deluxe) insert

### Day Before:

1. Cut 1" grid in thick fat part to break it up and get seasoning down in there.
2. Rub meat with rub of choice. Cover with saran wrap and throw in fridge.
3. Get grill ready by filling 3/4 of the slow 'n sear with charcoal. Don't fill to the brim so wood chunks can still fit in there. Water trough with water.
4. Options: place pan in grill below where meat will go, fill part way with water.

### Cooking day:

5. Light charcoal in chimney. Only need like 1/3 or 1/2 full of charcoal.
6. When ready, dump lit charcoal at one end of the slow 'n sear.
7. Place smoking wood chunks on lit and unlit charcoal - spacing them out a couple inches.
8. Place meat over water-pan if using. Let cook with vents fully open at first. Will need to close vents 80% eventually. Try to maintain temp of 225. That low is usually hard for me. I can maintain around 275 more easily. Maybe I'm not closing vents enough or too much air leaks in. 
9. Every 3 hours check the charcoal and wood chunks. Use a metal tool to knock the ashes out of the slow 'n sear, then you might need to put more new charcoal in if it's a long cook.
10. After the meat has been on half the cook time, take it off the grill, wrap in heavy duty foil or a couple layers of regular aluminum foil. Place back on grill. Or, optionally, stop using grill and transfer to a 275 degree oven covered/wrapped for the rest of the cook time
11. Continue grilling/baking another four to six hours.
12. Take meat off grill (or out of oven) and let rest for at least 20 min, up to 1 hour in the foil in a baking pan.
13. Shread meat, make sandwiches!!


## Cabbage slaw ingredients

This is just for a small amount. Can be doubled of course, but this amount is good for our family.

- 1 cup shredded red cabbage
- 1 cup shredded green cabbage
- 1 carrot, peeled and shredded
- 1/2 red onion, thinly sliced
- 1/2 jalapeño, seeds removed, minced
- Zest and juice of 1/2 lime
- 2 T rice vinegar
- 1 T extra-virgin olive oil
- Kosher salt and freshly ground black pepper


# Below is the instructions for snake method

## Directions

### Day Before:

1. Cut 1" grid in thick fat part to break it up and get seasoning down in there.
2. Rub meat with rub of choice. Cover with saran wrap and throw in fridge.
3. Get grill ready by placing a ring of charcoal around slightly more than half of the grill. This is the 'snake method' which can googled.
4. Place pan in grill below where meat will go, fill part way with water, put in smoking wood chunks.

### Cooking day:

5. Light charcoal in chimney. Only need like 1/3 or 1/2 full of charcoal.
6. When ready, dump burning charcoal at one end of the snake.
7. Place now-wet smoking wood chunks on _first half_ of snake (since second half will not be for smoke, just slow-cooking while meat is in foil).
8. Place meat over water-pan (not over charcoal). Let cook with vents fully open at first. Will need to close vents 80% eventually. Try to maintain temp of 225 or something.
9. Four to six hours later, take meat off, wrap in heavy duty foil or a couple layers of not heavy duty foil. Place back on grill.
10. Continue cooking another four to six hours.
11. Take meat off grill and let rest for at least 20 min, up to 1 hour in the foil in a baking pan.
12. Shread meat, make sandwiches!!
