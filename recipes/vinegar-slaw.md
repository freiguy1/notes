Kaytee and I made this coleslaw for fish tacos one night and perhaps liked the slaw more than the tacos! This goes really well with savory sandwiches, like pulled pork!

## Ingredients

- 1 cups shredded red cabbage
- 1 cups shredded green cabbage
- 1 carrots, peeled and shredded
- 1/2 red onion, thinly sliced
- 1/2 jalapeño, minced
- Zest and juice of 1/2 lime
- 1 T rice vinegar
- 1 T apple cider vinegar
- 1 tablespoons extra-virgin olive oil
- Kosher salt and freshly ground black pepper to taste

## Directions

1. Just mix it all
