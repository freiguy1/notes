# Roasted Brussels Sprouts
This is a recipe for delicious brussel sprouts which are roasted in the oven

### Ingredients:
- Salt
- Pepper
- Vegetable oil
- 1lb brussel sprouts

### Instructions:

1. Clean up brussel sprouts
2. Mix brussel sprouts in bowl with salt, pepper and oil
3. Bake in oven for 35 minutes at 375 degrees
4. Enjoy!
