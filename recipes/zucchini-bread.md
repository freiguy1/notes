This bread was made by Jess for the 2024 Frei Family Reunion, and my family loved & gobbled it up. I tried it once, but it turned out too dense, especially near the bottom. Below is a modified recipe that I think should do a better job next time.

# Ingredients
3/4c sugar (lowered from 1c)
1/2c oil
1 egg + 1 egg white
1tsp vanilla
1.5c flour
1/2tsp cinammon
1/2tsp baking soda
1/2tsp salt
1/4tsp baking powder
1/2c grated & drained (squeeze all moisture out) zucchini
1/4c nuts (optional)


# Directions
1. Oven to 350.
2. Beat sugar and oil until blended.
3. Add eggs and vanilla, stir well.
4. Add sifted ingredients alternately with zucchini.
5. Stir well
6. Optionally add nuts.
7. Bake for 1-1.5h. Took me about 1h 15 min.

