# Homemade French Freis (Fries)

![](/assets/images/fries.jpg)

The key to recipe is uniformity: fry size consistency & spacing them uniformly while baking. Our kids are kind of hit-or-miss with restaurant/fast food french fries, but they will consume these babies!

Cook time is pretty sensitive to fry size, and even potato species. Yukon golds cook faster than russet. Smaller fries cook faster than larger. Due to soak time and cook time these can take an hour or more to make, so you need to prepare ahead of time.

### Ingredients

- 3-4 potatoes
- 3T olive oil
- salt (or seasoned salt)
- pepper
- garlic
- other assorted seasonings of your choice
    - For example: italian seasoning, vegetable seasoning, dill, lemon pepper

### Instructions

1. Wash potatoes with paper towel
2. Cut potatoes into fries! I prefer 1/4"x1/4" or a wee bit smaller.
3. Soak potatoes for 30 minutes in a large bowl. This extracts some starch which will lead to crunchier fries.
4. Drain water then rinse potatoes one more time with water.
5. Preheat oven to 425F (use convection if available)
6. Dry fries! We do this with a tea towel: toss the fries around inside the folded towel. Also dry bowl.
7. Put the dried fries back in the bowl, add olive oil, toss, add seasonings as desired, toss.
8. Spread fries so they aren't touching on oven-safe cookie drying rack.
9. Cook for 17-35 minutes (depends on oven, potato, fry-size; usually around 20 minutes for me). Keep an eye on them! They will eventually get crispy & brown, but a couple minutes too long and they'll be black and charred!

