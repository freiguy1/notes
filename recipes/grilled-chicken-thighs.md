# Grilled Chicken Thighs

I grilled these for Oscar's first birthday party and people were in love. They were so complimentary with them! [Inspiration video](https://youtu.be/es-yKVNnDUk?si=EhjeoJ7kPk8u04ul).

### Ingredients
- 14 seasoned chicken thighs (seasoned from the store, 14 fit on the grill)
- 1 cup bbq sauce


### Instructions
1. Get a large amount of charcoal started. I think I did 3/4 charcoal chimney full.
2. Throw on one side of grill (I used my 'Slow n Sear')
3. Open bottom vents fully, top vent only 25% open. Wait 5 min for temp to rise up.
4. Place all thighs on grill - indirect heat.
5. Place a baseball-sized chunk of oak on the hot coals
6. After an hour, flip thighs. Check temp. We're looking for ~175 F.
7. Check temp every 15 min for 175. Add 5-15 charcoal if temp is dropping below 350. I stayed around 400 for this bbq.
8. Once reached 175, baste with bbq sauce.
9. Go another 20-30 minutes til the bbq sauce has caramelized. May need to flip thighs in between.
10. I seared the non-skin side for a few seconds over the coals before taking off.
11. Rest for 20 min, serve.
