# Date Rolls

During the Christmas season Ethan really loves his mom's fruitcake. It's a demanding recipe though, and Ethan thought it'd be cool to make something with the same vibe quickly and affordably.

[Original Recipe](https://blog.joolies.com/date-nut-roll-recipe)

### Ingredients

- 1/4 cup raw almonds
- 1/4 cup raw cashews
- 1/4 cup pistachios
- 1 1/2 tbsp sesame seeds
- 2 1/4 cup (medjool if possible!) dates, pitted
- 1 tbsp coconut oil
- 1/2  tsp cinnamon 
- 1/4 tsp nutmeg

### Instructions

1. Roughly chop almonds, cashews, and pistachios then transfer to an 8" cast iron pan (if available) and place on medium heat to lightly toast (3-4 minutes). Next add the sesame seeds to the pan and toast for an additional minute or two. Remove from heat and set aside.
2. Grab a cutting board and roughly chop the pitted dates then add chopped dates and coconut oil to the large pan. Cook on low heat (the dates will get softer/mushy) then add the toasted nuts, cinnamon, and nutmeg to the pan and continue to mix over low heat for a few minutes.
3. Remove from heat and allow it to cool. If needed, mix further then transfer to a large sheet of parchment paper (or cling wrap; do not use wax paper, it sticks!) and roll into a log. Place in fridge to cool further.
4. Slice to 3/8" thick, but don't let them get TOO cold. They're very hard to chop if frozen or near.

### Notes

You can instead of those individual nut types, you can get a can of mixed nuts and use those. Or a bag of pistachios + can of mixed nuts.