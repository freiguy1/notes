# Rice Pudding

![](../../assets/images/rice-pudding.jpg)

### Ingredients

- 3/4 cup uncooked white rice
- 2 cups milk, divided
- 1/4 cup white sugar
- 1/4 teaspoon salt
- 1 egg, beaten
- 2/3 cup golden raisins (optional)
- 1 tablespoon butter
- 1/2 teaspoon vanilla extract

### Directions

1. Bring 1 1/2 cups water to a boil in a saucepan; stir rice into boiling water. Reduce heat to low, cover, and simmer for 20 minutes.
2. In a clean saucepan, combine 1 1/2 cups cooked rice, 1 1/2 cups milk, sugar and salt. Cook over medium heat until thick and creamy, 15 to 20 minutes.
3. Stir in remaining 1/2 cup milk, beaten egg, and optional raisins; cook 2 minutes more, stirring constantly.
4. Remove from heat and stir in butter and vanilla.

[Source](https://web.archive.org/web/20171201195334/http://everybodylovesitalian.com/easy-creamy-rice-pudding-recipe/)
