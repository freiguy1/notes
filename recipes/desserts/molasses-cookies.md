# Molasses Cookies

Originally from [this website](https://www.food.com/recipe/memos-molasses-cookies-470290). Apparently they come from an old school Brer Rabbit Molasses sticker.

### Ingredients
- 3/4c shortening
- 3/4c sugar plus maybe a T extra
- 1/4c molasses
- 1 large egg
- 2c flour
- 2t baking soda
- 1/2t clove
- 1/2t ginger
- 1t cinnamon
- 1/2t salt

### Instructions
1. Melt shortening. Cool so as not to cook egg.
2. Add sugar, molasses and egg to melted shortening. Mix well.
3. Sift dry ingredients into a different bowl then mix them well.
4. Add paste ingredients to dry ingredients.
5. Chill thoroughly (in a chest freezer for an hour seems to do the trick).
6. Preheat to 350
7. Form into 1" or 1.25" balls, roll in granulated sugar, and place on cookie sheet covered with parchment paper 2-3 inches apart.
8. Bake for 12 ~~15 16~~ minutes. Cookies should be light, pillowy and soft. Not dark and crispy. Look for the tops to fissure a bit before removing.
9. Cool on drying rack. Right out of the oven they'll be soft, but they'll harden as they cool.
10. Try not to eat too many in one setting

### Notes
- I added a pinch of cayenne to the dry ingredients. I kind of like it. Others were on the fence.
- Try not cooling it as long. This might lead to it melting to flatter, thinner, wider cookies when cooking.
- 2024-01-15 changed to 15 minutes because cookies could've been slightly softer.
- 2025-02-16 we recalibrated our oven to be more correct. Burned bottoms :(. Lowered to 12 minutes.

