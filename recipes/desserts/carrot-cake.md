# Carrot cake with maybe less sweetness than other recipes

I just tried this once tonight. Haven't even tasted it yet, but it looks great! And I wanted to write down my notes in case it was awesome.

## Ingredients
- 2/3 cup white sugar
- 3/4 cup brown sugar
- 3/4 cup oil (vegetable or canola)
- 1/2 cup sourdough discard
- 4 large eggs
- 2 tsp cinnamon
- 1/2 tsp nutmeg
- 2 tsp baking powder
- 1 tsp baking soda
- 1 tsp salt
- 2 cups flour
- 2 1/2 cups freshly grated carrots (was about 7 small/medium carrots for me)

_Frosting_
- 8 Oz Cream cheese (Softened)
- 1/2 Cup Butter (Softened)
- 1 Tsp Vanilla
- 1 1/2 Cups Powdered sugar

## Directions
1. Grate carrots then preheat the oven to 350 degrees. 
2. In a large bowl, whisk together your sugars, oil, and sourdough discard until well combined.
3. Add in the eggs and stir until just combined.
4. Dump in the cinnamon, nutmeg, baking powder, baking soda, salt, and flour. Stir into the mixture.
5. Fold in your grated carrots.
6. Pour mixture into a greased 9x13 pan.
7. Place into the preheated oven and bake for 35-40 minutes or until a toothpick comes out clean.
8. Remove from oven and allow to cool completely before frosting. (Place in the fridge to speed up cooling.)

_Frosting_
1. Beat your cream cheese in a large bowl over medium speed until smooth. 
2. Add in the butter and continue beating till well blended.
3. Add in the vanilla and powdered sugar and continue to mix until the frosting is totally smooth.
4. Spread over cooled cake.
