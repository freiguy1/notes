# Skillet Oatmeal Cookie

[Source](https://littlespoonfarm.com/sourdough-oatmeal-cookies/).
I've modified the original by halving every ingredient, removing raisins, adding pecans, switching to skillet, and other minor things.

### Ingredients:
- 4 tablespoons softened butter (room temperature)
- 1/4 cup (50 g) brown sugar
- 1/8 cup (25 g) granulated sugar
- 1/6 cup (50 g) sourdough starter discard
- 1 egg white
- 1/2 teaspoon vanilla extract
- 1 cups (80 g) oats
- 1/2 cup (60 g) all-purpose flour
- 1/4 cup chopped pecans
- 1/4 teaspoon ground cinnamon
- 1/4 teaspoon baking soda

### Instructions:
1. Oven to 325 F
2. Grease 8" cast iron pan with butter or spray
3. Mix oats, flour, pecans, cinnamon, and baking soda in a medium bowl.
4. In different bowl, combine brown & white sugar with softened butter until sugar begins to dissolve with spatula. I had to microwave for 10 sec.
5. Add sourdough starter, egg, vanilla extract to second bowl and stir 'til smooth.
6. Combine dry mixture with wet until just mixed. Can refigerate dough for up to 2 days or bake immediately.
7. Put dough in cast iron pan, flatten til it's almost touching edges (it'll spread more as it cooks).
8. Bake for 30 minutes.
9. Let cool for at least 10 min before diving in.
