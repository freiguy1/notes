# Pumpkin Bread Ring
This comes from the lovely Grandma Judy Siebert.

## Ingredients
- 3c Bisquick
- 1c sugar
- 1c brown sugar
- 1/4c margarine
- 4 eggs
- 1 (16oz) can pumpkin
- 1/4c milk
- 2t cinnamon
- 1/2t ginger
- 1/4t cloves
- 1/4t nutmeg

## Instructions

1. Grease & flour 12c. Bundt pen.
2. Preheat 350F.
3. Beat all ingredients in large bowl on low speed, scraping bowl constantly for 30 seconds.
4. Beat on medium speed scraping bowl occasionally for 3 minutes.
5. Spread in pan.
6. Bake for 50 min.
7. Cool 10 min in pan, release.
8. Drizzle with glaze.


# Glaze

## Ingredients
- 1/3c butter
- 2c powdered sugar
- 1 1/2t vanilla
- 2-3T hot water

## Instructions

1. Heat butter over medium heat until delicate brown.
2. Blend in powdered sugar and vanilla.
3. Stir in water until smooth.
