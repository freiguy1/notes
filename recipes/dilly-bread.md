# Dilly Bread

This recipe comes from Cowboy Kent Rollin's "A Taste of Cowboy". It's a very unique savory bread that is so delicious warm with some butter. Everyone seems to like it and the smell is hard to beat. Also this is the first homemade bread I've tried so if I can do it, you can too!

Prep time: 30 Minutes

Total time: 2 hours and 30 minutes makes 1 loaf

### Ingredients
- 1 cup cottage cheese
- 1/2 cup water
- 3 tablespoons butter, melted
- 2 1/2 cups all-purpose flour
- 2 tablespoons sugar
- 1 tablespoon minced yellow onion
- 2 teaspoons dill seeds
- 1/2 teaspoon baking soda
- 1 (4-ounce) package rapid-rise yeast
- 1 teaspoon salt
- 1 large egg, beaten

### Instructions
1. Butter a 9-x-5-x-2-inch loaf pan.
2. Stir the cottage cheese, water, and 1 tablespoon of the butter together in a medium saucepan over low heat until warmed. Set aside.
3. In a large bowl, combine 1 1/2 cups of the flour, the sugar, onion, dill seeds, salt, baking soda, and yeast.
4. Slowly pour the cottage cheese mixture into the flour mixture. Add the egg and whisk until combined.
5. Stir in the remaining 1 cup flour until combined.
6. Cover the bowl with a tea towel and let rise in a warm place until nearly doubled in size, approximately 40 min utes.
7. Remove the towel and lightly knead the dough down. Place the dough in the loaf pan and cover with a buttered piece of wax paper. Let rise until doubled in size, about 40 minutes.
8. About 20 minutes before baking, preheat the oven to 350°F.
9. Remove the wax paper and bake for 30 to 40 minutes, or until the bread is golden brown and a toothpick inserted in the center comes out clean.
10. Let the loaf cool slightly before removing from the pan and brushing with the remaining 2 tablespoons melted butter. Serve warm or at room temperature.
