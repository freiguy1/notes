# Quick pickled red onions

My wife's mom sent this our way in Feb 2021, and we really enjoy these pickled red onions on sandwiches, salads, pizzas, quesadillas and the like.

### Ingredients

- 2 red onions
    - if baseball-sized or a bit smaller, they'll fit in 1 pint jar
    - if larger than baseball-sized, might need to use 1.5 or just 1 red onion
- 1/2c vinegar
- 1/2c water
- 1 1/2T sugar (tablespoon!)
- 1 1/2t salt (teaspoon!)
- Optional 2 cloves garlic
- Optional 1/2T peppercorns

### Instructions

1. Mix marinade (vinegar, water, sugar, salt) together in small pan and heat for a minute until solids are dissolved.
2. While allowing the mixture to cool (as to not cook onions when poured over), slice onion in half, then slice thinly (1/16" - 1/8") for half-circles.
3. Pack onion in a pint jar, then (optional) peppercorns and garlic on top or distributed throughout.
4. Pour the not-scalding-hot marinade into the jar submersing all food in jar.
5. Put lid on and place in refrigerator for a day, then enjoy. 

Keeps in fridge up to 3 or 4 weeks


