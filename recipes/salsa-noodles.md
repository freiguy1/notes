# Grandma's Parmesan Salsa Noodles

My Grandma Patsy made this dish frequently for Sunday dinner after church. I think I would sometimes just fill my plate up with only these despite all the other choices.

### Ingredients

- 1 lb fettuccini noodles
- 6T butter
- 2/3c salsa (she used Pace Picante mild or medium, I think)
- 2/3c grated Parmesan cheese
- 1c sour cream
- 1/4t salt

### Instructions

1. Cook noodles.
2. Combine butter, salsa, Parmesan, and salt in sauce pan.
3. Cook on low heat, stirring until butter is melted.
4. Stir in sour cream and salt.
5. Drain noodles and add sauce.
6. Toss.
7. Sprinkle with parsley if desired

-----------------
## Quoted from the email!

I don't mind at all! I got it from "Mr. Food" on Channel 13's noon show. 

Fettuccini Noodles

1# Fettuccini noodles
6T. butter 
2/3 c. Picante sauce, mild
2/3 c. grated Parmesan cheese
1 c. sour cream
1/4 t. salt

Cook noodles. Combine butter, Picante sauce, Parmesan cheese and salt in sauce pan. Cook on low heat, stirring until butter is melted. Stir in sour cream and salt. Drain noodles and add sauce. Toss. Sprinkle with parsley if desired. 

Enjoy, G&G
