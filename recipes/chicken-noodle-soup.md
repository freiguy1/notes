# Instant pot chicken noodle soup

Currently probably favorite recipe in the pressure cooker. We are convinced it has healing properties.

[Link to original recipe](https://www.cookingclassy.com/instant-pot-chicken-noodle-soup/#jump-to-recipe)

### Ingredients
- 1 Tbsp olive oil
- 1 1/2 cups peeled and diced carrots (3 medium)
- 1 1/2 cup diced celery (2 large stalks)
- 1 cup chopped yellow onion (1 small)
- 3 tsp minced garlic (3 cloves)
- 8 cups low-sodium chicken broth
- 2 tsp minced fresh thyme* (or just add in 2 large sprigs) (or 1 tsp dried)
- 2 tsp minced fresh rosemary (or just add 1 large sprig) (or 1 tsp dried, chopped finely with knife)
- 1 bay leaf
- Salt and freshly ground black pepper
- 2 1/2 lbs bone-in, skin-on chicken thighs, skinned*
- 3 cups wide egg noodles, such as American Beauty
- 1 Tbsp fresh lemon juice
- 1/4 cup chopped fresh parsley

### Instructions
1. Select the "saute" setting on the Instant Pot and let heat until the label reads "hot."
2. Drizzle in olive oil then add in carrots, celery and onion saute 2 minutes. Add garlic and saute 1 minute longer.
3. Add in broth, thyme, rosemary, bay leaf and season with salt and pepper to taste, then add in chicken. Press "cancel" on Instant Pot turn turn off saute mode.
4. Cover and close lid and make sure the vent on the lid is set to "sealing" position.
5. Select "manual" (or high pressure on other pressure cookers) and set to 10 minutes.
6. Once time is up let the pressure come down naturally for 10 minutes then carefully use the quick release method to release any remaining pressure.  
7. Remove chicken and transfer to a cutting board. Press "cancel" on the Instant pot then press "saute."
8. Add noodles and let cook, uncovered, until noodles are tender, about 5 - 6 minutes. 
9. Meanwhile shred chicken. Once noodles are done, stir in lemon juice, parsley and chicken, serve warm.
10. *I prefer this soup without the skin, it's much less greasy. But the bones add flavor so don't just go for boneless thighs. To skin the thighs simply start from one corner and pull the skin off, use a knife to trim away any portion that is stuck on.
