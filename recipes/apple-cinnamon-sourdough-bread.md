<style>
    kf-notes {
        filter: brightness(65%);
    }
</style>

# Apple Cinnamon Sourdough Bread

#### Personal Notes After First Loaf
Used ~100g (out of the 500g for the actual recipe) of King Arthur All Purpose Flour because we ran out of bread flour. About 1¾ Honey crisp apples. Fed around 11:30pm night before (50g starter, 75g flour and water), started making the dough around 10. Bulk rise until about 7 pm. Had the dough on the stove with the oven on keep warm for some of the day.

### Ingredients

#### Apple Cinnamon Mixture
- ½ tablespoon butter
- 1 tablespoon lemon juice or water
- 2 apples <kf-notes>peeled and diced into ¼-inch cubes</kf-notes>
- 2 tablespoons brown sugar packed
- ½ teaspoon ground cinnamon
- ⅛ teaspoon ground nutmeg

#### Dough
- 500 grams (4 cups + 3 tablespoons) bread flour
- 325 grams (1 ¼ cups + 2 tablespoons) warm water 80-90℉
- 100 grams (½ cup) active sourdough starter
- 10 grams (2 ½ teaspoons) kosher salt
- 2 ice cubes for steam

### Instructions
1. Feed your sourdough starter about 6-12 hours before starting your loaf. In a 75℉ kitchen, the starter should peak in about 6 hours.
2. Melt butter in a small skillet over medium heat. Add the diced apples and lemon juice and cook for 14 minutes until slightly softened. Add the brown sugar, cinnamon, and nutmeg. Stir to combine. Cook for an additional 5 minutes until the sugar thickens to a syrup-like consistency, add a pat of butter if it seems dry. Remove from heat and let cool completely.
3. In a large mixing bowl, combine the bread flour, warm water, active sourdough starter, and kosher salt. Mix until a cohesive dough forms and no dry flour remains. Cover the bowl and let the dough rest for 30 minutes.
4. Spread ¼ of the apple mixture over the top of the dough. Pull one side of the dough up and over the apples. Rotate the bowl 90 degrees and add another ¼ of the apple mixture, pulling that side up and over the apples. Repeat until all the apple mixture is incorporated and the dough has been folded over itself 4 times. Gather the dough into a ball and flip it so the folded side is on the bottom. Cover and rest for 30 minutes.
5. Repeat the stretching and folding process three more times, allowing the dough to rest for 30 minutes between each set.
6. Cover the dough and let it ferment until it has doubled in size and bubbles form on the surface. This usually takes about 3-4 hours in a 75℉ kitchen.
7. Turn the dough out onto a lightly floured surface. Shape it into a rectangle and fold one long edge over the middle, then the other long edge over that, like you're folding a letter into thirds. Tug both short edges up over the middle. Flip the dough seam side down and shape it into a tight ball. Cover and let it rest for 15 minutes.
8. Flip the dough seam side up again and repeat the shaping process. Shape it into a boule or batard, depending on your proofing basket. Place the shaped dough, seam side up, into a lightly floured proofing basket, pinching to seal the seam. Cover and refrigerate for 12-16 hours or overnight.
9. Preheat your oven to 450℉. Once the oven reaches temperature, preheat the Dutch oven for 5 minutes.
10. Transfer the dough to a piece of parchment paper or a bread sling. Score the dough as desired. Carefully remove the Dutch oven from the oven and add two ice cubes to create steam. Gently lower the scored bread into the Dutch oven using the parchment paper or bread sling. Cover and bake for 40 minutes. Remove the lid and bake for an additional 5 minutes or until the crust reaches your desired color.
11. Remove the bread from the oven and let it cool completely (at least 2 hours) on a wire rack before slicing and serving.

#### OTHER NOTES
Storage: Store bread at room temperature for up to 2 days, then freeze for up to 3 months.

[Source](https://enwnutrition.com/apple-cinnamon-sourdough-bread/#recipe)