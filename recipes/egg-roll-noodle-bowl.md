# Egg roll noodle bowl

One of our absolute favorites! Found it in a Taste of Home catalog. Like the internals of egg rolls made into a noodley dish!

### Ingredients
- 1 T sesame oil
- 1 lb ground pork
- 1 T soy sauce
- 1 clove garlic minced
- 1 t salt
- 1/2 t turmeric
- 1/2 t pepper
- 1/2 t curry powder
- 6 cups shredded cabbage (about 1 small head)
- 2 large carrots shredded (about 2 cups)
- 4 oz thin rice noodles
- 3 green onions thinly sliced (optional, but use regular onions otherwise)
- serve with additional soy sauce & seasonings


### Directions
1. In a large skillet, heat oil over medium-high heat. Add pork; cook 4-6 minutes or until browned breaking into crumbles. Drain if needed.
2. Stir in soy sauce, garlic, and seasonings.
3. Add cabbage, white part of green onion, and carrots, cook for 4-6 minutes longer or until veggies are tender, stirring occasionally.
4. Cook rice noodles according to package directions
5. Drain and immediately add to pork mixture, tossing to combine.
6. Sprinkle with green part of green onions.

