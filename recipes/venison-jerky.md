# Venison Jerky
## This is experimental

I'm working on a good home-made recipe for venison jerky.

### Instructions:

1. Mix up marinade
2. Slice up meat (we did a bit of 2# this time, I think it'd work w/ ~ 2#) (easier when slightly frozen - 1/8" thick)
3. Combine the two
4. Let sit for 8 hours
5. Spread out on dryer and dry for 2.5 hrs at 160 deg.  Thicker pieces give extra time.
6. Pieces should be flexible but not too squishy when being taken out.
7. For extra bacteria-killing precaution for meat which hasn't been frozen a month, throw in oven on baking tray at 300 for 10 min.

Note: Put frozen meat in fridge for ~18h and it will be half-thawed and easily cut-able into thin strips.

### Marinade:

- 1/4c soy sauce
- 1/4c a1 sauce
- 1 tbsp maple syrup
- 1 tbsp worchestershire sauce
- 1 tbsp sriracha
- 1/2 tbsp liquid smoke
- some pepper
- some garlic powder

### Thoughts:

Wasn't spicy at all. A bit of sweetness from maple syrup. I think this marinade amount would be good for 2 lbs of meat. For over 2 lbs, it's maybe a little light. I added some water to the marinade so it'd fill the bowl better.

Second try: I did it with beef. It was kind of over salty, for some reason. Perhaps too much soy? For v2, I'm cutting on soy and adding more maple syrup.

### Marinade v2:

- half 1/4c soy sauce
- 1/4c a1 sauce
- 3 tbsp maple syrup
- 1 tbsp worchestershire sauce
- 5 drops daves insanity
- 1/2 tbsp liquid smoke
- some pepper
- some garlic powder


Not enough saltiness with 1/8c soy sauce. Adding that back in and adding bbq sauce.

### Marinade v3:

- a little less than 1/4c soy sauce
- 1/4c a1 sauce
- 3 tbsp maple syrup
- 1 tbsp worchestershire sauce
- 5 drops daves insanity
- 2 tbsp bbq sauce
- 1/2 tbsp liquid smoke
- some pepper
- some garlic powder

Thoughts: Seems pretty good. Gary F. tried it and said it tastes mostly like A1, and I think I agree.

### Marinade v4 (2022):
Removing bbq, adding syrup, adding liquid smoke

- 1/4c soy sauce
- a little less than 1/4c a1 sauce
- 4 tbsp maple syrup
- 1.5 tbsp worchestershire sauce
- 5 your spiciest hot sauce
- 1 tbsp liquid smoke
- some pepper
- some garlic powder
- add water 1 tbsp at a time, after meat is marinated, so liquid is touching all meat. Don't do too much right away.

_Note: Didn't keep/write down results_

### Marinade v5 (2024):
Doing slightly different ratios. Using like 3# meat

- 1/3c soy sauce
- a little less than 1/4c a1 sauce
- 1/4c maple syrup
- 2.5 tbsp worchestershire sauce
- a couple tbps of homemade fermented hot sauce
- some pepper
- some garlic powder
- add water 1 tbsp at a time, after meat is marinated, so liquid is touching all meat. Don't do too much right away.
