# Swiss Steak Casserole with Butter Crumb Dumplings
Classic Grandma Kathy recipe, a Quast Family Fav

### Ingredients

#### Steak and Gravy
- 3 lbs round steak
- 1/3 cup flour
- 2 cans Cream of Mushroom Soup
- 1 can of Cream of Chicken Soup
- 1 can of water
- 1/2 tsp salt
- 1/8 tsp pepper

#### Dumplings/Biscuits
- 1 cup plain(fine) bread crumbs
- 3/4 cup melted butter, divided
- 4 cups flour
- 6 tsp baking powder
- 2 tsp poultry seasoning 
- 1 tsp salt
- 2 cups milk

### Instructions

#### Steak and Gravy
Preheat oven to 300deg
1. Cut meat into steaks (thin) and dredge in flour.
2. Lightly sear in skillet.
3. Transfer meat to 2 quart baking dish.
4. Combine in skillet 2 cans of mushroom soup, 1 can of water, salt and pepper. Heat until boiling and then pour over meat in the baking dish.
5. Bake 2.5 hours. Prep dumplings (instructions below) about 45 minutes before this 2.5 hours is up.
6. At some point near the end make extra gravy with 1 can Cream of Chicken soup, about 1 can of water, and about 3-3.5 Tbs of soy sauce. Heat on stove.
7. Remove from oven and increase temp to 425. 
8. Place dumplings on top of meat and bake 20-30 minutes. Half will fit on top of the meat, bake the other half separately on greased parchment paper for about 12 minutes.


#### Dumplings
1. Mix bread crumbs and 1/4 cup of butter and set aside.
2. In a separate bowl, combine flour, baking powder, poultry seasoning and salt.
3. Pour in milk and remaining 1/2 cup of butter into flour mixture and stir until moistened; mixture will not be smooth. 
4. Drop by rounded spoonful into buttered crumbs from step 1. Roll until coated with crumbs.