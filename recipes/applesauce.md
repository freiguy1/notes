# Homemade Instant Pot Applesauce

### Ingredients
- About 16-18 medium sized apples
- 1 tsp cinnamon
- 1/4 tsp nutmeg
- 1/4 cup sugar
- 1/4 tsp salt
- 1 cup water

#### Blueberry Vanilla Variation
**(Leave out the nutmeg and add the following for the blueberry variation)**
- 2 cups of blueberries
- 1 Tbsp vanilla
- 1 Tbsp lemon juice


Peel and cut apples into large chunks and place in the pot, they should fill instant pot to about the top measurement line on the pot. Add the other ingredients (we put the blueberries in the middle after adding half of the apples). Put the cover on and turn vent to "Sealing". Press the manual button and set to 5 minutes. Let the cycle run and when over, press the cancel button. Let it naturally release for fifteen minutes. After that, release the rest of the pressure and remove the cover. Stir it up and let cool. Strain or immersion blend if wanted.
