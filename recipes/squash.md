# Squash
Just a recipe to make some squash, like from mom's garden. Okay we did this for the first time last night and for some reason it was amazing. My family is typically not squash eaters, but this recipe was very good with a buttercup squash.

### Ingredients

- squash (buttercup or butternut, I think we like buttercup more)
- olive oil
- a few pinches salt
- 1T packed brown sugar
- 1/2t-1t cinnamon (not sure correct amount, see what it looks like)
- Water
- 2T Butter

### Instructions

1. Preheat oven to 400F - try 375 and maybe a bit longer time? It's a little dark since the oven temp was recalibrated...
2. Cut squash in half symmetrically
3. Use spoon to scoop out seeds and scrape inside clean
4. Stab the flesh-side with fork some
5. Spread a bit of olive oil on inside
6. Lightly salt inside
7. Put a bit of water over parchment paper in bottom of baking sheet
8. Put squash on water/parchment paper flesh-side down
9. Poke holes in outside of squash for steam release
10. Put in oven for 60m. (might need to go longer)
11. Take out and remove flesh from skin
12. Mix in 2T butter, 1T brown sugar and some cinnamon
13. Maybe use immersion blender if consistency is uneven
14. Eat!
