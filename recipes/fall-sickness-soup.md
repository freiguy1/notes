# Fall Sickness Soup

I made this on September 17th, 2023 when our kids and Kaytee were sick with colds. I was literally running to the garden grabbing multiple vegetables and to the basement for potates. I threw them in a pot hoping for the best, and the kids could not get enough (even Oliver!). I did over-salt it, so beware of that. My fix was to just dilute with more water. 

"I saw you actually running"
- Kaytee, right now

It's kind of just a brothy chickeny tomatoey comfort soup for when people are feeling ill - or whenever, of course. Kaytee said she thinks it's kinda like ministrone, and I agree. It utilizes a bunch of ingredients I (and others I know) grow in my garden, so though the list is long, done in late Aug or Sep, it's not too bad.

### Ingredients

- 1/2 T vegetable oil
- 1/2 medium or whole small onion chopped petitely
- 1/2 red bell pepper chopped petitely
- 2 cloves garlic minced
- 6c chicken broth (can be made with bouillon; i used 6c water + 2T 'better than bouillon' paste)
- 2c water
- 2 large potatoes or 4 small potatoes peeled & chopped into 1/2" cubes
- 1/2c - 3/4c finely chopped cabbage
- 2 peeled/chopped small carrots
- 2 blanched, peeled, finely chopped tomatoes
- 1t Italian seasoning
- 1/4t pepper (or just whatever you prefer)
- salt to taste
- _Optional:_
    - other favorite soup-ey seasonings (celery salt? thyme? rosemary?)
    - Kids might like some noodles added (but not much, keep it brothy!)
    - Got a chicken thigh or couple legs laying around from last Wednesday's rotisserie? Throw it in shredded, even the bone. Not too much though!

### Instructions

1. Oil and onion in hot dutch oven, sautee 5 min.
2. Bell pepper in dutch oven, sautee 5 more min.
3. Garlic in dutch oven, sautee 2 more min
4. Add water, broth, potatoes, carrots, cabbage, tomatoes, italian seasoning, pepper and other seasonings (basically just everything else) to pot.
5. Scrape bottom of pot to incorporate all the goodness stuck to bottom from sauteeing.
6. Bring to boil, simmer for 20-30 min (however long you have).
7. Taste, add salt if needed (broth can be salty already).

