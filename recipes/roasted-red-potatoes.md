# Roasted Red Potatoes
If you have some red potatoes, all the other ingredents can be found in the house. A solid one for just making a nice easy side.


### Ingredients

- 4-7 red potatoes (not baby)
- 1/4c olive oil
- salt (or seasoned salt)
- pepper
- other assorted seasonings of your choice
    - For example: italian seasoning, garlic, vegetable seasoning

### Instructions

1. Preheat oven to 450F
2. Wash & dry potatoes with paper towel
3. Chop potatoes into 1" or 3/4" chunks & put in bowl
4. In bowl, add 1/4c olive oil, toss to distribute
5. Sprinkle with salt, pepper, and seasonings of choice
6. Spread on baking sheet lined with aluminum foil
7. Put in oven for 40 minutes.

### Other thoughts
Add sweet potatoes or carrots for some more diversity.

