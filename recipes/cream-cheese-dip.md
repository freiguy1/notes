# "Man Dip"

### Ingredients

- 2 packages of cream cheese
- 1 can of Rotel
- 1 Hot Jimmy Dean Sausage

### Instructions

1. Warm up cream cheese so it's not cold.
2. Drain Rotel
3. Brown sausage
4. Mix in small crockpot
5. ...
6. Enjoy and impress
