i had the hankering for a soup made with sauerkraut. Grandma Judy said they always made pork ribs with sauerkraut. A little googling lead me to a Polish soup called [Kapusniak](https://www.budgetbytes.com/kapusniak/). This recipe is the inspiration for mine, however mine is an all-day cook rather than quick boiling like the recipe.

## Ingredients V2

- 3 lb pork ribs which were cut like 1" long. 4ish rib bones per slice
- 6 c water
- 3 bay leaves
- 4/5 med/small potatoes chopped dice-size
- 6 slices bacon
- 2 small yellow onions chopped
- 3 med/large carrots chopped
- 2 cloves garlic minced
- 3 c sauerkraut
- 1 tsp salt
- 0.5 tsp pepper
- sour cream garnish

## Instructions

1. In the morning add water, pork ribs, bay leaves, and potatoes to slow cooker. Put on low setting. This should go for 9-10 hours total.
2. 2.5 hours later, fry/bake bacon how you prefer. Fry a little crisper so it stays somewhat crisp in the soup.
3. Chop bacon fairly small, add half to slow cooker, save rest for garnish.
4. In the bacon grease you've just created sautee onions, carrots, and garlic for 8 minutes. Then add to slow cooker.
5. 3 hours from completion remove ribs, slice between each bone, place back in slow cooker.
6. Add sauerkraut, salt, and pepper to slow cooker.
7. When complete, remove bay leaves. Serve with salt, pepper, leftover chopped bacon, sour cream.

## Jan 19, '25 Thoughts
So I updated the ingredient list to v2. Adding ribs, bacon, sauerkraut. Removing a quart of water.

Results: This is way better than the first time. Putting in cold kraut 3h from end was maybe too late. Soup was warm but homemade kraut was still semi-crunchy. We should try different pork cut as well. Something with the bones in.


## Dec 16, '24 Thoughts

Honestly, a good soup! Flavor is a little mild, but that might be intentional as an Eastern-European dish. Kaytee said it could use more meat (pork ribs). I thought it wasn't too low on meat. Adding another pound might be way too much. I definitely think it needs more than 2.5c sauerkraut. The recipe said 4. That's what we should try next. The sour cream was a necessity for me. Turned it into a creamy smooth broth instead of watery. This came right up to the top of my 5qt slow cooker, so be careful adding more without removing something else. Perhaps potatoes and water could go down a bit.

One of the greatest things is the cost of this soup. Using pork riblets is extremely cost-effective with the obvious drawback of spitting out a couple dozen little bones. All ingredients are cheap especially with home-made sauerkraut (I wouldn't have it any other way).

Lots of fat renders off the pork. When cooled, the soup has a 1/8" floating translucent layer of the stuff! If one is so inclined, one could skim the fat off. Other than the aforementioned fat, it seems like a very healthy dish, and I felt perfectly fine eating to my heart's content.

I'm excited to see what round 2 brings for this recipe!
