# Pork Wontons

My mom always made these around New Years for our church's "Watchnight" service! They were always one of my favorites, and I specifically remember dipping them in a A1+soy sauce slurry.

At this point this recipe is an experiment because until now, it's just been off the cuff.

### Ingredients

**Meat & Veggies & Stuff**
- 1lb ground pork sausage browned (try separating as much as possible!)
- 1 - 1 1/4c carrots chopped finely (2-3 medium carrots)
- 1/2 large onion chopped finely
- 2 inches ginger root peeled and chopped finely
- 1/8 cabbage head chopped finely (1 1/2 - 2c)
- 4-6 garlic cloves (depending on size) chopped finely
- 1c finely chopped snap peas (keep pods!)
- 1 package of wonton wraps
- 1c oil

**Marinade**

- 3T soy sauce
- 1T packed brown sugar
- 1T fish sauce (optional)

**Dipping Sauce (make however much needed)**
- 2T soy sauce
- 2T A1 sauce

### Instructions

1. Mix pork, veggies, ginger, garlic in ziplock plastic bag.
2. Mix marinade together and add to plastic bag (can store this plastic bag in the fridge for a week or so).
3. Put filling in wonton wraps and fold however you want, use wet fingers on edges to make them sticky.
4. Get oil hot in pan (cast iron of course! (or whatever, of course)). The oil just needs to be a quarter or half inch deep; no need to fully submerge.
5. Cook wontons in hot oil on both sides until they're golden brown.
6. Dry on paper towel and let cool.
7. Enjoy with dipping sauce!
