# Creamy, Cheesy Wild Rice Soup
Developed by Cassie Stayton

## Our Version

![](../assets/images/wild-rice-soup.jpg)

### Ingredients:

- 3/4 cup wild rice
- 1 medium onion, diced
- 3 cloves garlic, minced
- 32 oz chicken broth (low sodium)
- 8 slices bacon (maple/honey)
- dash of pepper
- 6 oz marbled cheese (colby/jack), cubed
- Small (8oz) container of velveeta, cubed
- 1 super large or 2 chicken breasts, cubed
- 8oz half & half
- 1/2c water

### Instructions

1. Simmer wild rice, garlic, onion, broth, pepper and 2 slices of uncooked bacon for 60 min (some wild rices take  more like 75 min) covered.
2. Fry rest of bacon until crisp (a bit longer than how I like breakfast bacon). Baking it works very well. 400 degrees for 30 min, check at 25.
3. When simmering is 10 min from completion, put in cubed chicken breast.
4. When simmering complete, add cheeses, heavy whipping cream, water and crumbled cooked bacon.
5. Remove uncooked bacon, its job is complete.
6. Keep heating until all cheeses are melted.
7. ...
8. Profit.

---
---
### Cassie's Original recipe 

#### Ingredients
- 3/4 cup wild rice
- 1 med/large onion
- 3 med cloves of garlic
- 30 oz chicken broth
- 2 slices of uncooked bacon
- Dash of pepper

#### Instructions

Simmer above for about 60 minutes

Fry 6 pieces of bacon until crisp
When rice mixture is done, add heavy whipping cream, velveeta cheese, cheddar cheese, and colby cheese, to desire flavor.

Hint: You can cut up 2 or 3 chicken breasts, into cubes, and add to rice mixture when rice is nearly cooked.  Just so the chicken has time to cook.  10 minutes?
