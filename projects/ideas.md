# Physical (and maybe software)
- Dice rolling box (drop in top, hits a couple platforms on the way down, dice end up in felted box)
- Folding chair/stool (design challenge)
- Electric box joint jig
- Drill & charger station - done
- Weather wall: show next week of weather with light-up cut-outs. Temp indication will be relative to previous day
- E-ink recipe book
- Plant-starting greenhouse
- More professional/finished moon lamp
- Remote-controlled snow plow robot

# Software only
- Dairy farming simulator game
- Add search feature to notes website

# Done
- Pallet wood headboard with sconces (and tv remote) - partially done (no tv remote)
