## Notes for lawncare

# 2024

I spread 2 40# bags of some generic fertilizer over the front half 4/25 or something. Bought from the country store by the library. I don't remember the exact composition of the fertilizer, but it was recommended by the lady at the counter. $20/bag.

I spread 3/4 bag chicken poo fertilizer over the full garden, heavier on the new side of the garden 5/6

Sprayed apple trees 6/20

Planted apple trees 6/25. North one is "Snowsweet" and south one is "Kinderkrisp"



# 2016

#### Fertilizer

- Spreader: $50
- 3x Superior Weed and Feed 10,000sqft. $14-18 each
- Went 5mph w/ fourwheeler and covered both sides in front and west side in back

#### Mulch

- Re-mulched all places with dark walnut color mulch.
- Needed 2 yd total. Truck carries 1.5 yd.
- $30/yd

#### Flowers

- 7 bulb flowers for left and right of walkway: $8/ea
- Yet to purchase: wheelbarrow flowers, 2 hanging for front (ferns?), 2 by garage
- more to come...

#### Mower

- Bought 70 oz of oil: $15
- One filter: $9
- Changed oil

