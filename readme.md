- [x] Account for PDFs. There's one in /gaming/dnd
- [x] Create a breadcrumb
- [x] deploy
- [x] sort folder items
- [ ] style this baby
- [x] dark/light mode & switch & remember for browser

### Chota Screen Breakpoints:
- sm: 600px
- md: 900px
- lg: 1200px
