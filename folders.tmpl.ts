export const layout = "layouts/folder.njk";

interface ChildItem {
  name: string;
  type: "folder" | "note" | null;
  url: string | null;
}

const ignore = ["_data.ts", "deno.json", "_includes", "_config.ts", "_site", "deno.lock", "folders.tmpl.ts", "assets", "readme.md", ".gitignore", ".git", ".gitlab-ci.yml", "public"];

export default async function* ({ search }) {
  const searchResults = search.pages("");
  const folders = new Set(searchResults
    .map(sr => sr.src.entry.path)
    .map(path => path.substring(0, path.lastIndexOf("/") + 1))
  );


  for (const folder of folders) {
    const children = [];
    for await (const dirEntry of Deno.readDir(`.${folder}`)) {
      if (folder === "/" && ignore.includes(dirEntry.name)) continue;

      const fullPath = `${folder}${dirEntry.name}`;
      const page = searchResults.find(sr => sr.src.entry.path === fullPath);
      let type = null;
      let url = page?.data.url ?? "";
      if (dirEntry.name.endsWith(".md")) {
        const fullPath = `${folder}${dirEntry.name}`;
        type = "note";
      } else if (dirEntry.isDirectory) {
        type = "folder";
        url = `${folder}${dirEntry.name}/`;
      } else {
        url = `${folder}${dirEntry.name}`;
      }
      children.push({ name: dirEntry.name, type, url });
    }

    children.sort((a, b) => {
      if (a.type === "folder" && b.type !== "folder") return -1;
      else if (a.type !== "folder" && b.type === "folder") return 1;
      else return a.name.localeCompare(b.name);
    });

    yield {
      layout,
      content: folder,
      url: folder,
      children,
      data: {
        children
      },
    };
  }
}

/*
 Page {
    src: {
      path: "/content/wishlist",
      slug: "wishlist",
      asset: undefined,
      lastModified: 2023-11-06T01:53:22.713Z,
      created: 2023-11-06T01:53:22.713Z,
      remote: undefined,
      ext: ".md",
      entry: Entry {
        name: "wishlist.md",
        path: "/content/wishlist.md",
        type: "file",
        src: "/home/freied/dev/deno/notes/content/wishlist.md",
        children: Map(0) {},
        flags: Set(0) {}
      }
    },
    data: {
      mergedKeys: { tags: "stringArray" },
      paginate: [Function: paginate],
      search: Search {},
      tags: [],
      content: "### General\n" +
        "\n" +
        "- [Saw file for 14tpi dovetail saw](http://www.leevalley.com/us/Wood/page.aspx?p=69854&"... 894 more characters,
      url: "/content/wishlist/",
      date: 2023-11-06T01:53:22.713Z,
      page: [Circular *97]
    }
  }*/
