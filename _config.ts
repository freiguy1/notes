import lume from "lume/mod.ts";

const site = lume();

site.copy("assets");

site.copyRemainingFiles();

export default site;
